using System;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace XMRSMS{
    public class AlertBox{
        public static async Task<ContentDialogResult> DisplayAlertBox(string title, string message)
        {
            ContentDialog alertDialog = new ContentDialog
            {
                Title = title,
                Content = message,
                CloseButtonText = "Ok"
            };

            return await alertDialog.ShowAsync();
        }
    }
}