using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;

namespace XMRSMS{

    public class DashboardThreadButtons{

        public static void doNavigate(string btnName, string userID){



            ThreadViewData data = new ThreadViewData();
            data.peer = ulong.Parse(btnName);
            data.userID = userID;
            data.existing = true;

            Frame rootFrame = Window.Current.Content as Frame;
            rootFrame.Navigate(typeof(ThreadViewPage), data);

        }

    }

}