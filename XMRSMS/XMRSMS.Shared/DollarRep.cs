using System;
namespace XMRSMS{

    public class DollarRep{

        public static string GetDollarRep(long bal){
            double dollar;
            long cents;
            dollar = Math.Floor((double)(double) bal / (double)100);
            cents = (long) bal % 100;
            return $"{dollar}.{cents}";
        }

    }

}