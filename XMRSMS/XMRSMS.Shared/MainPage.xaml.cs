using System;
using System.Threading.Tasks;

using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Popups;

using monerosms;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace XMRSMS
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>

    
    public sealed partial class MainPage : Page
    {
        private string errorTextColor = "Yellow";
        private bool tryingAct = false;
        private async void DetectServerUp(){
            Console.WriteLine("Checking if server up");
            try{
                await Ping.PingUserServer(null);
                this.LoginGrid.Visibility = Visibility.Visible;
                this.serverUp.Visibility = Visibility.Collapsed;
                Console.WriteLine("Server up");
            }
            catch(TaskCanceledException){
                this.serverUp.Text = "Server down... try again later";
                this.serverUp.Foreground = errorTextColor;
            }
            catch(TimeoutException){
                this.serverUp.Text = "Server down... try again later";
                this.serverUp.Foreground = errorTextColor;
            }
        }
        public MainPage()
        {
            this.InitializeComponent();

            if (Constants.baseURI is null || Constants.baseURI.Length == 0){
                this.serverUp.Text = "No server baseURI set.";
                this.serverUp.Foreground = this.serverUp.Foreground = errorTextColor;
            }
            else if (!Constants.baseURI.StartsWith("http")){
                this.serverUp.Text = $"Invalid Server base URI {Constants.baseURI}";
                this.serverUp.Foreground = this.serverUp.Foreground = errorTextColor;
            }
            else{
                DetectServerUp();
            }
            
        }

        public async void OnIDGenBtnClick(){

            Console.WriteLine("Disabling");
            generateActBtn.IsEnabled = false;
            try{
                await GenerateAccount.DoGenerate();
            }
            catch (Exception e){
                Console.Error.WriteAsync(e.Message);
            }
            finally{
                generateActBtn.IsEnabled = true;
            }


        }
        public async void OnIDSetBtnClick(){

            try{
                await Login.DoLogin(userIDLoginInput.Text);
            }
            catch(System.Threading.Tasks.TaskCanceledException){
                await AlertBox.DisplayAlertBox("XMR SMS", "Server error... try again soon");
            }
            catch (System.Net.Http.HttpRequestException){
                await AlertBox.DisplayAlertBox("XMR SMS", "Server error... try again soon");
            }
            catch(TimeoutException){
                await AlertBox.DisplayAlertBox("XMR SMS", "No such active user");
            }


        }


    }
}
