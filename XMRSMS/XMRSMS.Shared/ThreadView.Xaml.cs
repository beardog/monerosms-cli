using System;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using monerosms;

namespace XMRSMS
{

    public sealed partial class ThreadViewPage : Page
    {
        public string userID;
        public ulong peer;
        private bool onViewPage = true;
        private List<string> messageIDs = new List<string>();

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            ThreadViewData threadInfo = (ThreadViewData) e.Parameter;

            this.userID = threadInfo.userID;
            this.peer = threadInfo.peer;

            threadName.Text = threadInfo.peer.ToString();
            doGetMessages();

        }

        public void OnThreadViewBackPress(){
            onViewPage = false;
            DashboardAccount accInfo = new DashboardAccount();
            accInfo.userID = userID;
            accInfo.login = true;
            Frame rootFrame = Window.Current.Content as Frame;
            rootFrame.Navigate(typeof(DashboardPage), accInfo);
        }
        public async Task<string> doGetMessages(){       
            ulong timestamp = 1;
            IEnumerable<Message> messages;
            while (onViewPage){
                messages = await GetMessages.GetMessagesByTimestamp(userID, peer, timestamp);
                timestamp = (ulong) (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;
                foreach(Message threadMsg in messages){
                    if (messageIDs.Contains(threadMsg.id)){
                        Console.Error.Write($"Accidentally gotten duplicate {threadMsg.id}");
                        Console.Error.FlushAsync();
                        continue;
                    }
                    messageIDs.Add(threadMsg.id);
                    TextBlock msg = new TextBlock();
                    var date = DateTimeOffset.FromUnixTimeSeconds(threadMsg.ts).ToString();
                    if (threadMsg.to == 1){
                        msg.Text = date + " me:\n" + threadMsg.txt;
                    }
                    else{
                        msg.Text = date + peer.ToString() + ":\n" + threadMsg.txt;
                    }
                    threadViewMain.Children.Add(msg);
                }
                await Task.Delay(3000);
            }
         return "";
        }

        public void OnSendPress(){

        }

        public ThreadViewPage()
        {
            this.InitializeComponent();
        }



    }
}
