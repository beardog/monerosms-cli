using System.Threading.Tasks;
using System;
using System.Net.Http;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;

using monerosms;

namespace XMRSMS
{

    public class GenerateAccount{

        public async static Task<string> DoGenerate(){
            // Create a new user and then login (go to dashboard)
            DashboardAccount accInfo = new DashboardAccount();
            accInfo.login = false;

            User user = new User();
            string newID = null;
            try{
                newID = await user.fetchNew();
            }
            catch (Exception e) when (e is HttpRequestException || e is TimeoutException)
            {
                await AlertBox.DisplayAlertBox("ERROR", "Could not generate account");
                return null;
            }
            accInfo.userID = newID;

            Console.WriteLine($"Generated account: {newID}");

            Frame rootFrame = Window.Current.Content as Frame;
            rootFrame.Navigate(typeof(DashboardPage), accInfo);
            
            return newID;
        }

    }

}