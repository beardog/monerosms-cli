using System.Threading.Tasks;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;

using monerosms;

namespace XMRSMS
{

    public class Login{


        public static async Task<string> DoLogin(string userID){
            Console.WriteLine(userID);
            DashboardAccount accInfo = new DashboardAccount();
            accInfo.userID = userID;
            accInfo.login = true;

            if (userID.Length != Constants.userIDLength){
                await AlertBox.DisplayAlertBox("XMR SMS", "Invalid user ID length");
                return "";
            }
            await Ping.PingUserServer(userID);

            Frame rootFrame = Window.Current.Content as Frame;
            rootFrame.Navigate(typeof(DashboardPage), accInfo);
            return "";
            

        }

    }

}