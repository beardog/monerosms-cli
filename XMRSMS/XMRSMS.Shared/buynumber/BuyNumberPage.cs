using System;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Net.Http;

using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Navigation;

using monerosms;


namespace XMRSMS
{

    public sealed partial class BuyNumberPage : Page
    {
        private string userID;
        private bool buying = false;
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            DashboardAccount accInfo = (DashboardAccount) e.Parameter;

            this.userID = accInfo.userID;
            loadPrice();
        }

        public async Task<int> loadPrice(){
            long price = (long) (await NumberShop.getVirtualNumberPrice(userID));
            numberPrice.Text = $"Virtual number price: ${DollarRep.GetDollarRep(price)} USD";
            return 0;
        }

        public async void doSearch(object sender, RoutedEventArgs myArgs){
            ushort counter = 0;
            ushort maxNumbers = 10;
            ulong[] numbers;
            try{
                numbers = await NumberShop.listNumbers(userID, ushort.Parse(areaCodeSearch.Text));
            }
            catch(System.Net.Http.HttpRequestException){
                await AlertBox.DisplayAlertBox("XMRSMS", "Could not load numbers, make sure it is a valid US area code.");
                return;
            }
            numberList.Children.Clear();
            foreach (ulong num in numbers){
                if (counter > maxNumbers){
                    break;
                }
                Console.WriteLine(num);
                counter += 1;
                Button btn = new Button();
                btn.Name = num.ToString();
                btn.Click += doBuyBtn;
                btn.Content = $"Buy {num}";
                numberList.Children.Add(btn);
            }
        }
        public async void doBuyBtn(object sender, RoutedEventArgs myArgs){
            if (buying){
                return;
            }
            string oldHeader = numberPrice.Text;
            
            buying = true;
            ulong number = ulong.Parse(((Button) sender).Name);
            numberPrice.Text = $"Attempting to buy {number}";
            Console.WriteLine($"Buying {number}");
            try{    
                await NumberShop.buyNumber(userID, number);
                await AlertBox.DisplayAlertBox("XMRSMS", $"Sucessfully bought {number}");
                doGoBackToDash();
            }
            catch(HttpRequestException){
                buying = false;
                await AlertBox.DisplayAlertBox("XMRSMS", "Error buying number. Try again soon");
                numberPrice.Text = oldHeader;
            }
        }
        public void doGoBackToDash(){
            DashboardAccount accInfo = new DashboardAccount();
            accInfo.userID = userID;
            accInfo.login = true;
            Frame rootFrame = Window.Current.Content as Frame;
            rootFrame.Navigate(typeof(DashboardPage), accInfo);
        }
        public void goBackToDash(object sender, RoutedEventArgs myArgs){
            doGoBackToDash();
        }

        public BuyNumberPage()
        {
            this.InitializeComponent();
        }

    }

}