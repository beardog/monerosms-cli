using System;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Navigation;

using monerosms;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace XMRSMS
{

    public sealed partial class DashboardPage : Page
    {
        List<ulong> threadList = new List<ulong>();
        
        public string userID;
        private bool onDashboard = true;
        private long userBal = 0;

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            DashboardAccount accInfo = (DashboardAccount) e.Parameter;
            
            if (accInfo.login){
                userName.Text = $"Welcome back, {accInfo.userID}";
            }
            else{
                userName.Text = $"Remember to save your ID, {accInfo.userID}";
            }
            this.userID = accInfo.userID;
            getThreads();
            setXMRAddress();
            getBal();
            getNumber();
        }

        public async Task<int> getNumber(){
            User user = new User();
            user.setID(userID);
            ulong num = await user.getNumber();
            if (num == 0){
                myNumber.Text = "No purchased number yet.";
            }
            else{
                dashboardMain.Children.Remove(BuyNumberBtn);
                myNumber.Text = num.ToString();
            }
            return 1;
        }

        public DashboardPage()
        {
            this.InitializeComponent();

        }
    }
}
