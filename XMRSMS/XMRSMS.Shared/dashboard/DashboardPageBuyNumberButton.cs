using System;

using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml;
using System.Threading.Tasks;

using monerosms;
namespace XMRSMS
{

    public sealed partial class DashboardPage : Page
    {
        public async Task<int> GoBuyNumber(){
            this.onDashboard = false;
            DashboardAccount data = new DashboardAccount();
            data.userID = userID;
            data.login = true;

            Frame rootFrame = Window.Current.Content as Frame;
            rootFrame.Navigate(typeof(BuyNumberPage), data);
            return 1;
        }
    }

}