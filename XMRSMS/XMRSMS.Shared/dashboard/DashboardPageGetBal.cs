using System;

using Windows.UI.Xaml.Controls;
using System.Threading.Tasks;

using monerosms;
namespace XMRSMS
{

    public sealed partial class DashboardPage : Page
    {
        public async Task<string> getBal(){
            long bal;
            double dollar;
            long cents;
            string result = "";
            while (true){
                try{
                    bal = await Balance.GetBal(userID);
                    //dollar = Math.Floor((double)(double) bal / (double)100);
                    //cents = (long) bal % 100;
                    result = DollarRep.GetDollarRep(bal);
                }
                catch (Exception e){
                    balLabel.Text = "Error getting balance.";
                    Console.Error.WriteLine(e.StackTrace);
                    await Console.Error.FlushAsync();
                    await Task.Delay(20 * 1000);
                    continue;
                }
                balLabel.Text = "Balance: $" + result + " USD";
                this.userBal = bal;
                await Task.Delay(60000);
            }
            return "";
        }
    }

}