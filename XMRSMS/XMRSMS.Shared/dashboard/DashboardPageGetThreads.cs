using System;

using Windows.UI.Xaml.Controls;
using System.Threading.Tasks;

using monerosms;
namespace XMRSMS
{

    public sealed partial class DashboardPage : Page
    {
        public async Task<string> getThreads(){
            ulong[] peers;
        
            while (this.onDashboard){
                try{
                    peers = await ThreadAPI.ListThreads(userID);
                }
                catch(Exception e){
                    Console.Error.WriteLine(e.StackTrace);
                    await Console.Error.FlushAsync();
                    await Task.Delay(20 * 1000);
                    continue;
                }
                
                foreach(ulong peer in peers){
                    if (! threadList.Contains(peer)){
                        threadList.Add(peer);
                        Button btn;
                        btn = new Button();
                        btn.Click += OnThreadBtnPress;
                        btn.Name = peer.ToString();
                        btn.Content = peer.ToString();
                        dashboardMain.Children.Add(btn);
                    }
                }
            if (threadList.Count == 0){
                threadsHeader.Text = "No threads yet";
            }
            await Task.Delay(3000);
         }
         return "";
        }
    }

}