using System;
using Windows.UI.Xaml;
using Windows.Foundation;
using Windows.UI.Xaml.Controls;
using Windows.Foundation.Collections;
using System.Threading.Tasks;

using monerosms;
namespace XMRSMS
{

    public sealed partial class DashboardPage : Page
    {
        public async Task<int> setXMRAddress(){
            try{
                moneroAddress.Text = await Requests.Get($"{userID}/moneroaddress");
            }
            catch (Exception e){
                moneroAddress.Text = "Error getting XMR address";
                Console.Error.WriteLine(e.StackTrace);
                await Console.Error.FlushAsync();
            }
            return 0;
        }
    }

}