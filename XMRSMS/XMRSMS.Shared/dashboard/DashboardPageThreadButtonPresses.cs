using System;

using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;
using System.Threading.Tasks;

using monerosms;
namespace XMRSMS
{

    public sealed partial class DashboardPage : Page
    {
        public async Task<int> NewThreadPress(object sender, RoutedEventArgs e){
            if (newNum.Text.Length == 0){
                return 1;
            }
            this.onDashboard = false;
            try{
                DashboardThreadButtons.doNavigate(newNum.Text, userID);
            }
            catch (System.FormatException){
                await AlertBox.DisplayAlertBox("XMR SMS", "Enter the number with no spaces or special characters.");
                this.onDashboard = true;
            }
            return 1;
        }
        
        public void OnThreadBtnPress(object sender, RoutedEventArgs e){
            var btn = (Button) sender;
            this.onDashboard = false;
            DashboardThreadButtons.doNavigate(btn.Name, userID);

        }
    }

}