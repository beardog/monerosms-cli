namespace monerosms;

public class Balance
{

    public static async Task<long> GetBal(string userID) {

        var req = await Requests.Get(userID + "/bal");
        return long.Parse(req);

    }

}
