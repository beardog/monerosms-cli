namespace monerosms;
public class Constants
{
    public static string assetsDir = "../../../../assets/";    

    public static string baseURI = Environment.GetEnvironmentVariable("xmrSMSServer");

    public static int userIDLength = 32;
}