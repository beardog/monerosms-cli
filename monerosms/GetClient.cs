namespace monerosms;
using System.Net;

public class GetClient {

    public static HttpClient GetClientWithProxyEnvVar(){

        var proxyAddress = Environment.GetEnvironmentVariable("xmrSMSProxy");

        HttpClientHandler handler = new HttpClientHandler();

        if (proxyAddress is not null && ! proxyAddress.Equals("")){
            handler.Proxy = new WebProxy(new Uri(proxyAddress));
        }

        HttpClient client = new HttpClient(handler);
        client.Timeout = TimeSpan.FromSeconds(120);
        return client;
    }

}