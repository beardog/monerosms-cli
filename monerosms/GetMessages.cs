namespace monerosms;
using System.Text.Json;
using System.Collections.Generic;

public class GetMessages
{


    public static async Task<IEnumerable<string>?> ListMessages(string userID, ulong peer) {
        string messageListRaw = await Requests.Get($"{userID}/list_message_ids/{peer}");
        JsonDocument messageListJSONDocument = JsonDocument.Parse(messageListRaw);
        return messageListJSONDocument.Deserialize<IEnumerable<string>>();
    }
    public static async Task<IEnumerable<string>?> ListMessages(string userID, ulong peer, ulong afterTimestamp) {
        string messageListRaw = await Requests.Get($"{userID}/list_message_ids/{peer}/{afterTimestamp}");
        JsonDocument messageListJSONDocument = JsonDocument.Parse(messageListRaw);
        return messageListJSONDocument.Deserialize<IEnumerable<string>>();
    }
     public static async Task<Message> GetMessage(string userID, ulong peer, string messageID){
         string rawMessage = await Requests.Get($"{userID}/get_message/{peer}/{messageID}");
         JsonDocument messageJSON = JsonDocument.Parse(rawMessage);
         return messageJSON.Deserialize<Message>();
     }

     public static async Task<IEnumerable<Message>> GetMessagesByTimestamp(string userID, ulong peer, ulong afterTimestamp){
         List<Message> messages = new List<Message>();
         var msgs = await ListMessages(userID, peer, afterTimestamp);
         foreach(string msgID in msgs){
             if (msgID.Length != 24){
                 throw new Exception(msgID.Length.ToString() + " not correct length");
             }
             messages.Add(await GetMessage(userID, peer, msgID));
         }
         return (IEnumerable<Message>) messages.ToArray();

     }


}
