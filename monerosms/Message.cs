using System.Threading.Tasks;
using System.Text.Json;
namespace monerosms{

    public class Message{
        public string txt {get; set;}

        public byte to {get; set; }

        public uint ts{get; set;}

        public string id {get; set;} // base64, not worth the extra complexity to convert to/from bytes


    }

}