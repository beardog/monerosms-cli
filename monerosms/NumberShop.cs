namespace monerosms;

using System.Text.Json;

public class NumberShop
{

    public static async Task<ulong[]> listNumbers(string userID, ushort areacode){
        if (userID.Length != 32){
            throw new Exception("Bad id");
        }

        string numberListRequest = await Requests.Get(userID + "/get-available-numbers/" + areacode.ToString());

        var numberResponse = JsonDocument.Parse(numberListRequest);
        var numbers = numberResponse.Deserialize<ulong[]>();

        return numbers;

    }

    public static async Task<string> buyNumber(string userID, ulong number){

        var req = await Requests.Post(userID + "/buynumber/" + number.ToString());
        string res = await req.Content.ReadAsStringAsync();

        if (! res.Equals("success")){
            throw new HttpRequestException(res);
        }
        return res;
    }
    public static async Task<ulong> getVirtualNumberPrice(string userID){

        var res = await Requests.Get($"{userID}/get-virtual-price");

        return ulong.Parse(res);
    }


}