namespace monerosms;
using System.IO;
using System.Threading;
public class Ping
{

    public static async Task<String> PingUserServer(string? userID){

        string ping = "";
        int tries = 100;
        int counter = 0;

        while (! ping.Equals("pong!")){
            counter += 1;
            
            if (counter > tries){
                throw new TimeoutException();
            }

            if (userID is null){
                ping = await Requests.Get("/health-check");
            }
            else{
                ping = await Requests.Get(userID + "/ping");
            }
            Thread.Sleep(50);
        }

        return ping;
    }

}
