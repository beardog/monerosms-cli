using System.Threading.Tasks;
using System.Net.Http.Headers;
using System.Net;
namespace monerosms;
public class Requests
{
    private static readonly HttpClient client = GetClient.GetClientWithProxyEnvVar();

    public static async Task<String> Get(string url){
        if (! Constants.baseURI.StartsWith("http")){
            throw new UriFormatException("No valid server URL");
        }

        client.DefaultRequestHeaders.Accept.Clear();

        client.DefaultRequestHeaders.Add("User-Agent", "MoneroSMS CLI");
        
        var myReq = await client.GetStringAsync(Constants.baseURI + url);

        return myReq;

    }

    public static async Task<HttpResponseMessage> Post(string url){
        if (! Constants.baseURI.StartsWith("http")){
            throw new UriFormatException("No valid server URL");
        }
        client.DefaultRequestHeaders.Accept.Clear();

        client.DefaultRequestHeaders.Add("User-Agent", "MoneroSMS CLI");

        return await client.PostAsync(Constants.baseURI + url, null);


    }
}