using System.Threading.Tasks;
using System.Collections.Generic;
namespace monerosms{

    public class ThreadAPI{

        public static async Task<ulong[]> ListThreads(string userID){
            ulong[] threads = {};
            string req = await Requests.Get($"{userID}/threads");

            string[] newLineDelimitedThreads = (req).Split('\n');

            List<ulong> threadCopy = new List<ulong>();
            foreach(string thread in newLineDelimitedThreads){
                if (thread.Length > 0){
                    threadCopy.Add(ulong.Parse(thread));
                }
            }

            return threadCopy.ToArray();
        }

    }

}