﻿namespace monerosms;
using System.IO;

public class User
{
    private string id;
    private ulong phoneNumber = 0;

    public void saveData(string dataDirectoryPath){
        Directory.CreateDirectory(dataDirectoryPath);
        File.WriteAllText(dataDirectoryPath + "/userid", getID());

        File.WriteAllText(dataDirectoryPath + "/number", this.phoneNumber.ToString());
    }

    public void loadData(string dataDirectoryPath) {
        this.id = File.ReadAllText(dataDirectoryPath + "/userid");
        this.phoneNumber = ulong.Parse(File.ReadAllText(dataDirectoryPath + "/number"));
    }

    public string getID(){
        return this.id;
    }

    public void setID(string myID) {
        this.id = myID;
    }

    public void setNumber(ulong number){
        this.phoneNumber = number;
    }
    public async Task<ulong> getNumber(){
        if (this.phoneNumber.Equals(0)){
            var numberReq = await Requests.Get($"{this.id}/number");
            if (numberReq.Length > 0){
                this.phoneNumber = ulong.Parse(numberReq);
            }
        }
        return this.phoneNumber;
    }


    public async Task<string> fetchNew() {


        string get = await Requests.Get("generate_id");

        await Ping.PingUserServer(get);

        this.id = get;
       

        return this.id;
    }

}

