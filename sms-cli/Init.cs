using monerosms;
namespace smscli{

    public class Init{

        public static string createAccount(string dataDir){

            User user = new User();
            string userID = user.fetchNew();

            File.WriteAllText(dataDir + "/" + DataFiles.userIDFile, userID);
            return userID;
        }

        public static void setAccount(string dataDir, string userID){

            // Will raise a timeout exception if it fails
            Ping.PingUserServer(userID);
            File.WriteAllText($"{dataDir}/{DataFiles.userIDFile}", userID);

        }

    }


}