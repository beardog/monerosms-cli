﻿using smscli;
void checkArgCount(int count){

    if (args.Length != count){
        Console.Error.WriteLine("This command requires " + count.ToString() + " arguments");
        Console.Error.Flush();
        throw new IndexOutOfRangeException();
    }

}

string dataDir = Environment.GetEnvironmentVariable("moneroSMSDataDir");

if (dataDir is null){

    dataDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "/monero-sms";

}
try{
    Directory.CreateDirectory(dataDir);
}
catch(Exception e){
    Console.Error.WriteLine("Could not create/access application data directory:\n"  + e.Message);
    return 2;
}

if (args.Length == 0){
    return 0;
}

switch(args[0]){

    case "init":
        try{
            checkArgCount(2);
        }
        catch(IndexOutOfRangeException){
            return 1;
        }
        if (args[1].Equals("new")){
            try{
                Init.createAccount(dataDir);
                Console.WriteLine("ID set");
            }
            catch (Exception e){
                Console.Error.WriteLine("Could")
            }
        }
        else if(args[1].Equals("set")) {
            checkArgCount(3);
            Init.setAccount(dataDir, args[2]);
        }
        else{
            Console.Error.WriteLine("Must specify 'set <account>' or 'new'");
            return 1;
        }
    break;
    default:
    break;

}

return 0;