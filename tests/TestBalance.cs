using NUnit.Framework;
using System;
using System.Threading.Tasks;

using monerosms;
namespace tests;

public class Tests2
{
    [SetUp]
    public void Setup()
    {
    }

    [Test, Category("Balance")]
    public void TestBal()
    {
        Task.Run(async () =>
        {
            User user = new User();
            await user.fetchNew();
            Assert.AreEqual(await Balance.GetBal(user.getID()), (long) 0);
        //var balGet = Requests.Get("");
        //balGet.Wait();
        }).GetAwaiter().GetResult();
    }


}