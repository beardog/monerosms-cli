using NUnit.Framework;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using monerosms;
namespace tests;

public class TestGetMessages
{
    [SetUp]
    public void Setup()
    {
    }

    [Test, Category("GetMessage")]
    public void TestGetMessageMetadataOne()
    {
        Task.Run(async () =>
        {
            User user = new User();
            string id = await user.fetchNew();
            ulong peer = 636_275_0837;
            IEnumerable<string> messages;

            Task<String> setnum = Requests.Get($"{id}/setnumber/6363953433");
            await Requests.Get($"{id}/addbal");
            await setnum;

            messages = await GetMessages.ListMessages(id, peer);
            Assert.AreEqual(messages.Count(), 0);
            
            await Requests.Get($"{id}/handle_incoming?from={peer}&message=hey");

            messages = await GetMessages.ListMessages(id, peer);
            Console.WriteLine(messages.ElementAt(0));
            var message = await GetMessages.GetMessage(id, peer, messages.ElementAt(0));

            Assert.AreEqual(message.to, (byte) 1);

        }).GetAwaiter().GetResult();
    }
 
    [Test, Category("GetMessage")]
    public void TestGetMessageAfterTimestamp()
    {
        Task.Run(async () =>
        {
            User user = new User();
            string id = await user.fetchNew();
            ulong peer = 636_275_0837;
            IEnumerable<string> messages;

            Task<string> setnum = Requests.Get($"{id}/setnumber/6363953433");
            await Requests.Get($"{id}/addbal");
            await setnum;

            messages = await GetMessages.ListMessages(id, peer);
            Assert.AreEqual(messages.Count(), 0);
            
            Requests.Get($"{id}/handle_incoming?from={peer}&message=hey");
            Thread.Sleep(2000);

            ulong epoch = (ulong) (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;

            Thread.Sleep(1000);
            await Requests.Get($"{id}/handle_incoming?from={peer}&message=hey2");

            messages = await GetMessages.ListMessages(id, peer, epoch);
            var message = await GetMessages.GetMessage(id, peer, messages.ElementAt(0));

            Assert.AreEqual(message.to, (byte) 1);

            Assert.AreEqual(message.txt, "hey2");

        }).GetAwaiter().GetResult();
    }
}