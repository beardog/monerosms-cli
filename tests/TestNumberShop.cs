using NUnit.Framework;
using System;
using System.Threading.Tasks;

using monerosms;
namespace tests;

public class Tests3
{
    [SetUp]
    public void Setup()
    {
    }

    [Test, Category("NumberShop")]
    public void TestNumberShop()
    {
        Task.Run(async () =>
        {
            User user = new User();
            await user.fetchNew();
            var numbers = await NumberShop.listNumbers(user.getID(), (ushort) 636);

            Assert.IsTrue(numbers[0].ToString().StartsWith("636"));
        }).GetAwaiter().GetResult();
    }

    [Test, Category("NumberShop")]
    public void TestBuyNumber()
    {
        Task.Run(async () =>
        {
            User user = new User();
            await user.fetchNew();
            string backdoorBal = await Requests.Get(user.getID() + "/addbal");
            ulong number = (await NumberShop.listNumbers(user.getID(), (ushort) 636))[0];

            Assert.AreEqual(backdoorBal, "success");

            await NumberShop.buyNumber(user.getID(), number);
            var res = Requests.Get(user.getID() + "/number");

            Assert.AreEqual(res.Result, number.ToString());
        }).GetAwaiter().GetResult();




    }

}