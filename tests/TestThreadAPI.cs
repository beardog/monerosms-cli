using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net.Http;

using monerosms;
namespace tests;

public class TestThreadAPI
{
    [SetUp]
    public void Setup()
    {
    }

    [Test, Category("ThreadAPI")]
    public void TestThreadListIsEmpty()
    {
        Task.Run(async () =>
        {
            User user = new User();
            string id = await user.fetchNew();

            ulong[] threads = await ThreadAPI.ListThreads(id);
            Assert.IsTrue(threads.Length == 0);

            string newLineDelimThreads = await Requests.Get($"{id}/threads");

            List<ulong> actualThreadList = new List<ulong>();
            foreach(string line in newLineDelimThreads.Split('\n')){
                if (line.Length != 0){
                    actualThreadList.Add(ulong.Parse(line));
                }
            }
            Assert.IsTrue(actualThreadList.Count == 0);

        }).GetAwaiter().GetResult();
    }
    [Test, Category("ThreadAPI")]
    public void TestThreadListFirstMessageSentByUs()
    {
        Task.Run(async () =>
        {
            User user = new User();
            string id = await user.fetchNew();
            HttpClient client = new HttpClient();

            ulong peer = 636_275_0837;

            var content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("", "hello")
            });

            await Requests.Get($"{id}/setnumber/6363953433");
            await Requests.Get($"{id}/addbal");

            var response = await client.PostAsync($"{Constants.baseURI}{id}/send_message/{peer}", content);

            var ret = await response.Content.ReadAsStringAsync();
            Console.WriteLine("ret" + ret);

            ulong[] threads = await ThreadAPI.ListThreads(id);
            Console.WriteLine(threads.Length);
            Assert.IsTrue(threads.Length == 1);
            Assert.AreEqual(threads[0], peer);


        }).GetAwaiter().GetResult();
    }
    [Test, Category("ThreadAPI")]
    public void TestThreadListFirstMessageSentByThem()
    {
        Task.Run(async () =>
        {
            User user = new User();
            string id = await user.fetchNew();
            ulong peer = 636_275_0837;

            await Requests.Get($"{id}/setnumber/6363953433");
            await Requests.Get($"{id}/addbal");

            ulong[] threads = await ThreadAPI.ListThreads(id);
            Console.WriteLine(threads.Length);
            Assert.IsTrue(threads.Length == 0);

            await Requests.Get($"{id}/handle_incoming?from={peer}&message=hey");

            threads = await ThreadAPI.ListThreads(id);
            Console.WriteLine(threads.Length);
            Assert.IsTrue(threads.Length == 1);

            Assert.AreEqual(threads[0], peer);


        }).GetAwaiter().GetResult();
    }

}