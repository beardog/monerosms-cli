using NUnit.Framework;
using System.Threading.Tasks;
using System;
using System.IO;
using System.Threading;
using monerosms;
namespace tests;

public class Tests
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void TestSaveData()
    {
        File.Delete(Path.GetTempPath() + "/userid");
        File.Delete(Path.GetTempPath() + "/number");

        User user = new User();
        user.setID("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        user.setNumber((ulong) 6363953426);
        user.saveData(Path.GetTempPath());

        Assert.AreEqual(File.ReadAllText(Path.GetTempPath() + "/userid"), "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        Assert.AreEqual(File.ReadAllText(Path.GetTempPath() + "/number"), "6363953426");
    }

    [Test]
    public void TestLoadData()
    {
        File.WriteAllText(Path.GetTempPath() + "/userid", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        File.WriteAllText(Path.GetTempPath() + "/number", "6363953426");

        User user = new User();
        user.loadData(Path.GetTempPath());

        Assert.AreEqual(user.getID(), "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        Assert.AreEqual(user.getNumber(), 6363953426);

    }

    [Test]
    public void TestUserNew()
    {
        Task.Run(async () =>
        {
            User user = new User();
            await user.fetchNew();
            Assert.IsTrue(user.getID().Length == 32);
        }).GetAwaiter().GetResult();

    }
}